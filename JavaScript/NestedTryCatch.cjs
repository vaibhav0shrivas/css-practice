function CustomError(message) {
    this.value = "customError";
    this.message = message;
}

try {
    try {
        throw new Error('inner catch error');
    } finally {
        console.warn('finally');
    }
} catch (ex) {
    console.error(ex);
    console.trace("catch block trace");

}

let a = [1, 2, 3, 4, 4, 5, 4, 6, 6, 6, "43"];
console.table(a);
console.assert(1 < 2, "bruh");
console.assert(2 === '2', "You used === buddy");
console.trace("f - trace");
