// function ourCodeMakesAMistake(){

// }

// function doSomethingToGetAJavaScriptError(){

// }


function doSomethingErrorProne() {
    if (ourCodeMakesAMistake()) {
      throw new Error('The message');
    } else {
      doSomethingToGetAJavaScriptError();
    }
  }
  
  try {
    doSomethingErrorProne();
  } catch (e) {
    // Now, we actually use `console.error()`
    console.error(e.name); // 'Error'
    console.log("this is log")
    console.error(e.message); // 'The message', or a JavaScript error message
  }
  