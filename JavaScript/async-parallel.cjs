// const async = require("async");
// Async Example - all calls made in parallel
async.parallel(
  {
    twelve: function (callback) {
      strictAddition(2, 10, callback);
    },
    fiftythree: function (callback) {
      strictAddition(42, 11, callback);
    },
    six: function (callback) {
      strictAddition(23, -17, callback);
    },
  },
  function (err, results) {
    if (err) {
      console.log(err);
      return;
    }
    console.log(results); // {twelve: 12, fiftythree: 53, six: 6}
  }
);
