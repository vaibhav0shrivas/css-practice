let firstObject = [1, 2, 3, 4];

console.log(firstObject);

let secondObject = [...firstObject];

firstObject[0] = 10;

let thirdObject = JSON.parse(JSON.stringify(firstObject));

console.log("First Object after change", firstObject);
console.log("Second Object after change", secondObject);
console.log("Third Object after change", thirdObject);
