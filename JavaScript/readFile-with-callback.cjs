const fs = require("fs");

fs.readFile("/foo.txt", function (err, data) {
  if (err) {
    console.log("An error occured", err);
    return;
  }

  console.log(data);
});

fs.readFile("foo.txt", function (err, data) {
  if (err) {
    console.log("An error occured", err);
    return;
  }

  console.log(data);
});
